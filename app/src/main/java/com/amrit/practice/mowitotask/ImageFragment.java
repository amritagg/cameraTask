package com.amrit.practice.mowitotask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;

public class ImageFragment extends Fragment implements SurfaceHolder.Callback {

    Bitmap bitmap;
    Activity activity;
    ArrayList<int[]> points;

    ImageView imageView;
    Button ok, cancel, polygon, remove;
    String st;
    ArrayList<int[]> newPoints;
    HashSet<String> set;
    DrawView drawView;
    boolean isOn, removePoints;
    Bitmap originalBitmap;

    public ImageFragment(Activity activity, Bitmap bitmap, ArrayList<int[]> points) {
        this.activity = activity;
        this.bitmap = bitmap;
        this.points = points;
        originalBitmap = bitmap;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_image, container, false);

        newPoints = new ArrayList<>();
        set = new HashSet<>();
        isOn = false;
        removePoints = false;
        imageView = rootView.findViewById(R.id.image_view);
        ok = rootView.findViewById(R.id.ok);
        cancel = rootView.findViewById(R.id.cancel);
        polygon = rootView.findViewById(R.id.polygon);
        drawView = rootView.findViewById(R.id.draw_view);
        remove = rootView.findViewById(R.id.remove);

        Glide.with(this).load(bitmap).into(imageView);

        ok.setOnClickListener(view -> saveImage());
        cancel.setOnClickListener(view -> cancel());
        polygon.setOnClickListener(view -> polygonDefine());
        remove.setOnClickListener(view -> removePoint());

        init();
        return rootView;
    }

    private void removePoint() {
        if(polygon.getVisibility() == View.VISIBLE) polygon.setVisibility(View.INVISIBLE);
        else polygon.setVisibility(View.VISIBLE);

        drawView.removePoint();
        removePoints = !removePoints;
    }

    private void init() {
        ViewTreeObserver vto = drawView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                drawView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width = drawView.getMeasuredWidth();
                int height = drawView.getMeasuredHeight();
                drawView.init(height, width, points);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void polygonDefine() {
        if(!isOn){
            isOn = true;
            polygon.setText("New Polygon");
            remove.setVisibility(View.VISIBLE);
            return;
        }
        bitmap = originalBitmap;
        drawView.clear();
        Glide.with(this).load(bitmap).into(imageView);
    }

    private void updateBitmap(){
        ArrayList<Stroke> paths = drawView.getPaths();
        points = drawView.getPoint();

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStrokeWidth(15);
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setAlpha(0xff);

        Canvas canvas = new Canvas(bitmap);
        for(int[] a: points) canvas.drawPoint(a[0], a[1], paint);

        paint.setColor(Color.BLUE);
        for (Stroke s : paths) canvas.drawLine(s.x, s.y, s.lx, s.ly, paint);

        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.DITHER_FLAG));

    }

    private void saveImage() {
        String FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS";
        String name = new SimpleDateFormat(FILENAME_FORMAT, Locale.ENGLISH).format(System.currentTimeMillis());
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/CameraApp-Image");
        }

        OutputStream imageOutStream;
        Uri uri = activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

        updateBitmap();
        try {
            imageOutStream = activity.getContentResolver().openOutputStream(uri);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, imageOutStream);
            imageOutStream.close();
            st = "/storage/emulated/0/Pictures/CameraApp-Image/" + name + ".jpg";
            Toast.makeText(getContext(), "This image is saved", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            getParentFragmentManager().popBackStack();
        }
    }

    private void cancel() {
        Toast.makeText(getContext(), "Image discarded!", Toast.LENGTH_SHORT).show();
        getParentFragmentManager().popBackStack();
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) { }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) { }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) { }

}