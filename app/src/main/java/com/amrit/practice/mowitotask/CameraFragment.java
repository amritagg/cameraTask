package com.amrit.practice.mowitotask;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.camera.core.AspectRatio;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

public class CameraFragment extends Fragment implements SurfaceHolder.Callback {

    private static final String LOG_TAG = CameraFragment.class.getSimpleName();
    private final Activity activity;

    public CameraFragment(Activity activity) {
        this.activity = activity;
    }

    private ImageCapture imageCapture = null;
    private ExecutorService cameraExecutor;

    SurfaceHolder holder;
    Canvas canvas;
    Paint paint;
    ArrayList<int[]> points;
    Bitmap bitmap;
    Button photo, random;
    PreviewView previewView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_camera, container, false);

        points = new ArrayList<>();
        previewView = rootView.findViewById(R.id.view_finder);
        photo = rootView.findViewById(R.id.photo);
        photo.setOnClickListener(view -> takePhoto());
        random = rootView.findViewById(R.id.random);
        random.setOnClickListener(view -> random());

        SurfaceView surfaceView = rootView.findViewById(R.id.holder);
        surfaceView.setZOrderOnTop(true);
        holder = surfaceView.getHolder();
        holder.setFormat(PixelFormat.TRANSPARENT);
        holder.addCallback(this);

        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GREEN);
        paint.setStrokeWidth(15);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);

        cameraExecutor = Executors.newSingleThreadExecutor();

        startCamera();

        return rootView;
    }

    private void takePhoto(){
        bitmap = previewView.getBitmap();
        canvas = new Canvas(bitmap);

        ImageFragment imageFragment = new ImageFragment(activity, bitmap, points);
        getParentFragmentManager().beginTransaction()
                .replace(R.id.container, imageFragment)
                .addToBackStack(null)
                .commit();
    }

    private void random(){
        int height = previewView.getHeight();
        int width = previewView.getWidth();

        canvas = holder.lockCanvas();
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);

        int x = ThreadLocalRandom.current().nextInt(0, width);
        int y = ThreadLocalRandom.current().nextInt(0, height);
        points.add(new int[]{x, y});
        for (int[] a : points) canvas.drawPoint(a[0], a[1], paint);
        holder.unlockCanvasAndPost(canvas);
    }

    private void startCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(activity);

        cameraProviderFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                Preview preview = new Preview.Builder().build();

                CameraSelector cameraSelector = new CameraSelector.Builder()
                        .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                        .build();

                preview.setSurfaceProvider(previewView.getSurfaceProvider());

                cameraProvider.unbindAll();

                imageCapture = new ImageCapture.Builder()
                        .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                        .setJpegQuality(100)
                        .build();
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture);
            } catch (ExecutionException | InterruptedException e) {
                Log.e(LOG_TAG, "Error " + e);
            }
        }, ContextCompat.getMainExecutor(activity));

    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) { }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) { }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) { }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cameraExecutor.shutdown();
    }

}