package com.amrit.practice.mowitotask;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

public class DrawView extends View {

    private static final float TOUCH_TOLERANCE = 4;
    private float mX, mY, x, y;
    private int width, height;
    boolean firstTime = true;

    private final ArrayList<Stroke> paths = new ArrayList<>();
    private ArrayList<int[]> points;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private final Paint mBitmapPaint = new Paint(Paint.DITHER_FLAG);

    private boolean remove;

    public DrawView(Context context) {
        this(context, null);
    }

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArrayList<Stroke> getPaths() {
        return paths;
    }

    public void init(int height, int width, ArrayList<int[]> points) {
        this.width = width;
        this.height = height;
        this.points = points;
        remove = false;
        mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        invalidate();
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();

        mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);

        Paint paint = new Paint();

        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GREEN);
        paint.setStrokeWidth(15);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setAlpha(0xff);

        for (int[] a : points) canvas.drawPoint(a[0], a[1], paint);

        paint.setColor(Color.BLUE);
        for (Stroke s : paths) mCanvas.drawLine(s.x, s.y, s.lx, s.ly, paint);

        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.restore();
    }

    private void touchStart(float x, float y) {
        if (remove) {
            for (int[] a : points) {
                if (Math.abs(a[0] - x) <= 25 && Math.abs(a[1] - y) <= 25) {
                    points.remove(a);
                    firstTime = true;
                    break;
                }
            }
            return;
        }
        Stroke fp = new Stroke(x, y, x, y);
        paths.add(fp);
        this.x = x;
        this.y = y;

        mX = x;
        mY = y;
    }

    private void touchMove(float x, float y) {
        if (remove) {
            for (int[] a : points) {
                if (Math.abs(a[0] - x) <= 25 && Math.abs(a[1] - y) <= 25) {
                    points.remove(a);
                    firstTime = true;
                    invalidate();
                    break;
                }
            }
            return;
        }
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);

        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            paths.remove(paths.size() - 1);
            Stroke stroke = new Stroke(this.x, this.y, x, y);
            paths.add(stroke);
        }
    }

    public void clear() {
        paths.clear();
        invalidate();
    }

    public void removePoint() {
        remove = !remove;
    }

    public ArrayList<int[]> getPoint() {
        return points;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchStart(x, y);
                break;
            case MotionEvent.ACTION_MOVE:
                touchMove(x, y);
                break;
        }
        invalidate();
        return true;
    }

}
